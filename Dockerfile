FROM ubuntu:16.04

RUN apt-get update \
    && apt-get install -y -qq \
	   git curl \
       ssh \
       python-pip \
       python-setuptools \
       wget \ 
       tree \
       default-jre \
       graphviz \
       plantuml \
    && apt-get clean \
    && pip install mkdocs \
    && pip install pygments \
    && pip install pymdown-extensions \
    && pip install mkdocs-material \
    && pip install mkdocs-bootstrap \
    && pip install mkdocs-alabaster \
    && pip install mkdocs-cinder \            
    && pip install markdown-magic \    
    && wget -O plantuml.jar http://sourceforge.net/projects/plantuml/files/plantuml.jar/download \
    && curl -SL https://raw.githubusercontent.com/mikitex70/plantuml-markdown/master/plantuml.py > /usr/local/lib/python2.7/dist-packages/markdown/extensions/plantuml.py \
    
    
