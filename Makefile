



serve:
	docker run --rm -ti \
	--name site \
	-v ${PWD}:/src \
	-w /src \
	-p 9000:9000 registry.gitlab.com/build-images/mkdocs bash -c ' set -x; \
	    mkdir /mysite ;\
	    echo "construction Images png" ;\
	    java -jar /plantuml.jar -tsvg -o /src/docs /src/docs/uml/*.puml ;\
	    mkdocs build -d /mysite ;\
	    cd /mysite ;\
	    python -m SimpleHTTPServer 9000 ;\
	    bash ;\
	'

