# Mkdocs image

Image docker Mkdocs utilisée pour contruite un site à partir de fichiers en markown.  

Peut être utilisé avec Gitlab Page ou Cloudfoundry.
Plus d'information sur le site de [MkDocs](http://www.mkdocs.org/)


## Construire un site

On peux utiliser cet image pour construire un Site Static de documentation ou autre.

Ce repo inclus un site minimaliste pour en montrer l'usage, voir le fichier `.gitlab-ci.yml` pour l'utilisation.

> Un site de démo est déployé sur [GitLab Pages](https://build-images.gitlab.io/mkdocs)

## Dev du site

Vous pouvez utiliser cette image en local sur un poste de travail ou une VDI dans un docker pour construire et visualiser le rendu de votre site avant de le publier sur Gitlab.

Voici un exemple de commande :

```bash
	docker run --rm -ti \
	--name site \
	-v ${PWD}:/src \
	-w /src \
	-p 9000:9000 registry.gitlab.com/build-images/mkdocs bash -c ' set -x; \
	    mkdir /mysite ;\
	    echo "construction Images png" ;\
	    java -jar /plantuml.jar -tsvg -o /src/docs /src/docs/uml/*.puml ;\
	    mkdocs build -d /mysite ;\
	    cd /mysite ;\
	    python -m SimpleHTTPServer 9000 ;\
	'
```

il y a un Makefile qui permet d'executer cette commande automatiquement en executant la commande :

```
make serve
```

vous pouvez alors accéder a votre site sur l'url http://localhost:9000



## Contenu de l'image

Ce que l'image contient aujourd'hui :

- ssh
- git
- wget
- mkdocs 
- pygments 
- pymdown-extensions : http://facelessuser.github.io/pymdown-extensions/
- mkdocs-material
- default-jre
- plantuml
- graphviz

