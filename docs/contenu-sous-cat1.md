## Sous Catégory 1

juste du contenu.

### on peux aussi inclure des diagrammes UML générés lors du build


![uml](plantuml.svg)



### ou utiliser plantuml-markdown

exemple d'usage en ligne de commande `markdown_py -x plantuml mydoc.md > out.html`


et en rendu ici :

```
 ::uml:: format="png" classes="uml myDiagram" alt="My super diagram"
  Goofy ->  MickeyMouse: calls
  Goofy <-- MickeyMouse: responds
 ::end-uml::
```					
					
					
::uml:: format="png" classes="uml myDiagram" alt="My super diagram"
  Goofy ->  MickeyMouse: calls
  Goofy <-- MickeyMouse: responds
::end-uml::
