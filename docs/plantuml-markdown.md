
## PlantUML Markdown

This includes Plantuml for Python-Markdown https://github.com/mikitex70/plantuml-markdown

### utilisation plantuml-markdown

exemple d'usage en ligne de commande `markdown_py -x plantuml mydoc.md > out.html`


et en rendu ici :

```
 ::uml:: format="png" classes="uml myDiagram" alt="My super diagram"
  Goofy ->  MickeyMouse: calls
  Goofy <-- MickeyMouse: responds
 ::end-uml::
```					
					
					
::uml:: format="png" classes="uml myDiagram" alt="My super diagram"
  Goofy ->  MickeyMouse: calls
  Goofy <-- MickeyMouse: responds
::end-uml::
